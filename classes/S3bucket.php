<?php defined('SYSPATH') or die('No direct script access.');
require_once Kohana::find_file('vendor', 'autoload');
use Aws\S3\S3Client;


class S3bucket {
    protected $__client;
    protected $__region;
    protected $__log;

    public function __construct() {
        $config_file = Kohana::$config->load('aws');
        $this->__region = $config_file->region;
        $this->__client = S3Client::factory(
            array(
                'key'    => $config_file->AWS_ACCESS_KEY_ID, 
                'secret' => $config_file->AWS_SECRET_KEY,
                'region' => $this->__region
                )
        );
        $this->__log = Log::instance();
    } 

    public function new_bucket($bucket, $log=FALSE) {
        $bucket_name = $bucket['Bucket'];

        if ( ! $this->__client->doesBucketExist($bucket_name, TRUE) ) {
            if ( ! $this->__client->isValidBucketName($bucket_name) ) {
                $this->__log->add(Log::NOTICE, "Bucket name invalid.");
            }else{
                $this->__log->add(Log::NOTICE, "Creating bucket named {$bucket_name}");
                try {
                    $result = $this->__client->createBucket(array(
                        'Bucket' => $bucket_name,
                        'LocationConstraint' => $this->__region
                    ));
                    $this->__client->waitUntilBucketExists(array('Bucket' => $bucket_name));
                } catch (Exception $e) {
                    
                }
            }
        } else {
            $this->__log->add(Log::NOTICE, "Bucket '{$bucket_name}' already exists.");
        }

        if ( array_key_exists('Lifecycle', $bucket) ){
            $this->lifecycle($bucket);
        }

        if ( $log ) $this->__log->write();
    }

    public function lifecycle($bucket, $log=FALSE) {

        $result = $this->__client->putBucketLifecycle(array(
            'Bucket' => $bucket['Bucket'],
            'Rules'  => $bucket['Lifecycle']
        ));

        $this->__log->add(Log::NOTICE, "Bucket '{$bucket['Bucket']}' Lifecycle: Ok.");

        if ( $log ) $this->__log->write();
    }
}
