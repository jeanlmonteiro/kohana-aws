<?php defined('SYSPATH') or die('No direct script access.');
require_once Kohana::find_file('vendor', 'autoload');
use Aws\S3\S3Client;


class S3file {
    protected $__client;
    protected $__bucket;
    protected $__bucket_name;
    protected $__download_expires;

    public function __construct($bucket) {
        $config_file = Kohana::$config->load('aws');

        if ( array_key_exists($bucket, $config_file->buckets) ){
            $this->__bucket      = $config_file->buckets[$bucket];
            $this->__bucket_name = $config_file->buckets[$bucket]['Bucket'];
            $this->__download_expires = $config_file->buckets[$bucket]['DownloadExpires'];

        } else {
            $msg = 'Try: ';
            foreach ($config_file->buckets as $key => $value) {
                $msg.= ' ' . $key;
            }
            throw new Exception($msg);
        }

        $this->__client = S3Client::factory(
            array(
                'key'    => $config_file->AWS_ACCESS_KEY_ID, 
                'secret' => $config_file->AWS_SECRET_KEY,
                'region' => $config_file->region
                )
        );
    }

    public function doesObjectExist($key)
    {
        if ( ! $this->__client->doesBucketExist($this->__bucket_name, TRUE) ) {
            $s3bucket = new S3bucket();
            $s3bucket->new_bucket($this->__bucket);
        }

        return $this->__client->doesObjectExist($this->__bucket_name, $key);         
    }

    public function new_file($key, $local_file) {

        if ( $this->doesObjectExist($key) ){
            throw new Exception('Object exist.');
        }

        if ( array_key_exists('Keys_default', $this->__bucket) ){
            $object = $this->__bucket['Keys_default'];
        }
        $object['Bucket']     = $this->__bucket_name;
        $object['Key']        = $key;
        $object['SourceFile'] = $local_file;

        $result = $this->__client->putObject($object);

        // We can poll the object until it is accessible
        $this->__client->waitUntilObjectExists(array(
            'Bucket' => $this->__bucket_name,
            'Key'    => $key
        ));   
    }

    public function getObjectUrl($key)
    {
        return $this->__client->getObjectUrl($this->__bucket_name, $key, $this->__download_expires);
    }

    public function getObject($key, $local_file)
    {

        if ( ! $this->doesObjectExist($key) ){
            throw new Exception('Object doesn\'t exist.');
        }

        $result = $this->__client->getObject(array(
            'Bucket' => $this->__bucket_name,
            'Key'    => $key,
            'SaveAs' => $local_file
        ));

        return $result['Body']->getUri();
    }
}
